/**
 * Created by user on 16/5/18.
 */


var swiper = new Swiper('.swiper-container', {
    pagination: '.swiper-pagination',
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    paginationClickable: true,
    effect : 'fade',
    onInit:function(swiper){
        $(swiper.slides[0]).find('.animated').addClass('flip');
    },
    onTransitionStart:function(swiper){
        var lengths = swiper.slides.length-1;
        var activeIndex = swiper.activeIndex;
        $(swiper.slides).find('.flip').removeClass('flip');
        if(activeIndex === lengths){
            setTimeout(function () {
                $(swiper.slides[swiper.activeIndex]).find('.animated').addClass('flip');
            },300);
        }
    },
    onTransitionEnd: function(swiper){
        $(swiper.slides[swiper.activeIndex]).find('.animated').addClass('flip');
    }
});
